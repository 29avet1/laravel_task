<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// you can generate both groups and users with db:seed
// I used tempauth middleware to temporary log in first user from db, basically it should be 'auth' middleware

Route::group(['middleware' => ['tempauth']], function (){
    Route::get('/groups','GroupsController@index');
    Route::get('/groups/{group}','GroupsController@show');
    Route::put('/groups/{group}','GroupsController@update');
});

