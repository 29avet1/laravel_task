<?php

use App\Group;
use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 10)->create();

        $users = User::all();

        $randArrays = [
            [2 => ['admin' => true], 3, 4],
            [1, 7=> ['admin' => true], 9],
            [2, 5=> ['admin' => true], 6=> ['admin' => true]],
            [4=> ['admin' => true], 7, 7]
        ];

        $users->each(function ($user) use ($randArrays) {
            $user->groups()->sync($randArrays[rand(0,3)]);
        });

    }
}
