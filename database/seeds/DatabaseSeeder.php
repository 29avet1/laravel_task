<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::statement("SET foreign_key_checks = 0");

        DB::table('groups')->truncate();
        DB::table('users')->truncate();

        DB::statement("SET foreign_key_checks = 1");

        $this->call(GroupsSeeder::class);
        $this->call(UsersSeeder::class);

        Model::reguard();
    }
}
