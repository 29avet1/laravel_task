<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests\GroupRequest;
use Exception;
use Illuminate\Http\Request;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::all();

        return response(['groups' => $groups], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param Group $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        if ($group->private && auth()->user()->roleOnGroup($group) == 'guest') {
            return response([
                'message' => "{$group->title} is private group"
            ], 403);
        }

        return response(['group' => $group], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param GroupRequest|Request $request
     * @param Group $group
     * @return \Illuminate\Http\Response
     */
    public function update(GroupRequest $request, Group $group)
    {
        if (auth()->user()->roleOnGroup($group) != 'admin') {
            return response([
                'message' => "You don't admin permissions to {$group->title} group"
            ], 403);
        }

        if (!isset($request->private)) {
            $request->offsetSet('private', $group->private);
        }

        try {
            $group->update($request->only([
                'title',
                'description',
                'private'
            ]));
        } catch (Exception $e) {
            return response([
                'data' => [
                    'code' => $e->getCode(),
                ],
                'message' => $e->getMessage()
            ], 500);
        }

        return response([
            'data' => [
                'group' => $group,
            ],
            'message' => 'Group successfully updated'
        ], 200);
    }
}
