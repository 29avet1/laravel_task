<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected  $fillable = [
        'title',
        'description',
        'private'
    ];

    public function has(User $user)
    {
        if (empty($this->users()->find($user->id))) {
            return false;
        }

        return true;
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
